#ifndef SETUPWINDOW
#define SETUPWINDOW

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>
#include <QGroupBox>
#include <QRadioButton>
#include <QLineEdit>
#include <QFormLayout>
#include <QTextEdit>
#include <QCheckBox>
#include <QProcess>
#include "functions.h"

class SetupWindow : public QWidget
{
    Q_OBJECT

private:
    QGridLayout *mainLayout;
    QLabel *title;
    int slideNbr;

    QGroupBox *configDetection;
    QVBoxLayout *configDetectionLayout;
    QGroupBox *keyGathering;
    QVBoxLayout *keyGatheringLayout;
    QGroupBox *installActions;
    QVBoxLayout *installActionsLayout;
    QGroupBox *advancedOptions;
    QVBoxLayout *advancedOptionsLayout;

    // BOX CONFIG OPENVPN
    QLabel *openVPNDescription;
    QLabel *openVPNResults;
    QComboBox *openVPNFoundFolders;
    QPushButton *openVPNExecuteEvaluation;
    QPushButton *openVPNAddLocal;
    QPushButton *openVPNAddx64;

    // BOX CONFIG PKCS12
    QLabel *pkcsDescription;
    QRadioButton *pkcsSelectKey;
    QRadioButton *pkcsRetriveFromDeathstar;
    QFormLayout *deathstarLayout;
    QLineEdit *deathstarLogin;
    QLineEdit *deathstarPassword;
    QPushButton *deathstarExecute;
    QPushButton *pkcsBrowse;
    QLabel *pkcsKeyPath;
    QString pkcsPath;
    QString pw;
    QString uname;

    // BOX ADVANCED CONFIG
    QCheckBox *launchAtStartup;
    QCheckBox *addShortcut;
    QCheckBox *pkcsCopyCheck;
    QCheckBox *distinctConnections;

    // BOX CONFIG INSTALL
    QPushButton *launchInstall;
    QTextEdit *logWindow;
    QProcess *cmd1;
    QProcess *cmd2;

    void openVPNSearch();
    bool throwError(int const& errorCode, QString const& errorMsg);

private slots:
    void selectInstallEvent();
    void openVPNSearchEvent();
    void browseKeyEvent();
    void selectKeyEvent();
    void retriveKeyEvent();
    void installx64Event();
    void deathstarEvent();
    void deathstarFinishedCmd1();
    void deathstarFinishedCmd2();
    void doInstall();

public:
    SetupWindow(QWidget *parent = 0);
    ~SetupWindow();
};

#endif
