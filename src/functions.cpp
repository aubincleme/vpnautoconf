#include <QDir>
#include <QRegExp>
#include "functions.h"

QList<QDir*> searchForInstall() {
    QList<QDir*> ovpnDirs;
    QDir currentDir;
    QStringList foundDirs;

    QStringList searchDirs;
    searchDirs << "C:/Program Files/" << "C:/Program Files (x86)/";

    QStringList dirFilters;
    dirFilters << "Open VPN" << "OpenVPN";
    currentDir.setNameFilters(dirFilters);


    for (int i = 0; i < searchDirs.length(); i++) {
        currentDir.setPath(searchDirs[i]);
        if (currentDir.exists()) {
            foundDirs = currentDir.entryList();
            if (!foundDirs.isEmpty()) {
                for (int j = 0; j < foundDirs.length(); j++) {
                    ovpnDirs.append(new QDir(searchDirs[i] + foundDirs[j]));
                }
            }
            foundDirs.empty();
        }
    }

    return ovpnDirs;
}

bool validateInstall(QDir* const& directory) {
    if (directory->exists("bin/openvpn.exe")) // TODO : Implémenter un test plus complet
        return true;
    else return false;
}
