#-------------------------------------------------
#
# Project created by QtCreator 2015-03-26T18:15:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VPN_AutoConf
TEMPLATE = app
DEPENDPATH += .
INCLUDEPATH += .

RC_FILE = res.rc

win32 {
CONFIG += embed_manifest_exe
QMAKE_LFLAGS_WINDOWS += /MANIFESTUAC:level=\'requireAdministrator\'
}

SOURCES += main.cpp\
    functions.cpp \
    setupWindow.cpp

HEADERS  += \
    functions.h \
    setupWindow.h

DISTFILES += \
    res.rc
