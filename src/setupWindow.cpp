#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QDesktopServices>
#include <QtCore>
#include <stdlib.h>
#include <string>
#include <Windows.h>
#include "setupWindow.h"

SetupWindow::SetupWindow(QWidget *parent)
    : QWidget(parent)
{
    mainLayout = new QGridLayout;

    configDetection = new QGroupBox;
    configDetectionLayout = new QVBoxLayout;
    configDetection->setTitle("1 - Configuration d'OpenVPN");

    keyGathering = new QGroupBox;
    keyGatheringLayout = new QVBoxLayout;
    keyGathering->setTitle("2 - Configuration de la clé PKCS12");

    advancedOptions = new QGroupBox;
    advancedOptionsLayout = new QVBoxLayout;
    advancedOptions->setTitle("3 - Options avancées");

    installActions = new QGroupBox;
    installActionsLayout = new QVBoxLayout;
    installActions->setTitle("4 - Lancer la configuration");

    title = new QLabel("Le présent utilitaire vous permet d'installer et de configurer \n OpenVPN afin qu'il puisse se connecter au réseau de l'EISTI.");
    title->setAlignment(Qt::AlignCenter);

    // BOX CONFIG OPENVPN
    openVPNDescription = new QLabel("Recherche de la configuration OpenVPN sur votre ordinateur ... ");
    configDetectionLayout->addWidget(openVPNDescription);
    openVPNResults = new QLabel("");

    openVPNFoundFolders = new QComboBox();
    openVPNFoundFolders->addItem("Recherche en cours");
    configDetectionLayout->addWidget(openVPNFoundFolders);

    openVPNAddLocal = new QPushButton("Localiser une installation");
    configDetectionLayout->addWidget(openVPNAddLocal);

    openVPNAddx64 = new QPushButton("Installer OpenVPN pour 32 et 64 bits");
    configDetectionLayout->addWidget(openVPNAddx64);

    openVPNExecuteEvaluation = new QPushButton("Actualiser");
    configDetectionLayout->addWidget(openVPNExecuteEvaluation);

    // BOX CONFIG PKCS12
    pkcsDescription = new QLabel("Pour se connecter au réseau de l'EISTI, \nil est nécessaire que vous indiquiez l'emplacement de votre certificat PKCS12.\nVous pouvez récupérer ce certificat au sein de l'EISTI à l'adresse vpn.eisti.fr,\nou en SSH sur deathstar.eisti.fr");
    keyGatheringLayout->addWidget(pkcsDescription);

    pkcsSelectKey = new QRadioButton("Sélectionner le certificat sur l'ordinateur");
    pkcsSelectKey->setChecked(true);
    keyGatheringLayout->addWidget(pkcsSelectKey);

    pkcsBrowse = new QPushButton("Parcourir");
    keyGatheringLayout->addWidget(pkcsBrowse);

    pkcsRetriveFromDeathstar = new QRadioButton("Télécharger le certificat depuis Deathstar");
    keyGatheringLayout->addWidget(pkcsRetriveFromDeathstar);

    pkcsPath = "";

    deathstarLayout = new QFormLayout;
    deathstarLogin = new QLineEdit();
    deathstarLogin->setEnabled(false);
    deathstarPassword = new QLineEdit();
    deathstarPassword->setEchoMode(QLineEdit::Password);
    deathstarPassword->setEnabled(false);
    deathstarExecute = new QPushButton("Exécuter");
    deathstarExecute->setEnabled(false);
    deathstarLayout->addRow("Nom d'utilisateur", deathstarLogin);
    deathstarLayout->addRow("Mot de passe", deathstarPassword);
    deathstarLayout->addRow(deathstarExecute);
    keyGatheringLayout->addLayout(deathstarLayout);

    pkcsKeyPath = new QLabel("Aucune clé sélectionnée.");
    pkcsKeyPath->setAlignment(Qt::AlignCenter);
    keyGatheringLayout->addWidget(pkcsKeyPath);

    // BOX CONFIG INSTALL
    launchInstall = new QPushButton("Lancer la configuration");
    installActionsLayout->addWidget(launchInstall);

    logWindow = new QTextEdit();
    logWindow->setReadOnly(true);
    installActionsLayout->addWidget(logWindow);

    cmd1 = new QProcess();
    cmd2 = new QProcess();


    // BOX ADVANCED OPTIONS

    launchAtStartup = new QCheckBox("Lancer OpenVPN au démarrage de l'ordinateur");
    launchAtStartup->setEnabled(false);
    advancedOptionsLayout->addWidget(launchAtStartup);
    addShortcut = new QCheckBox("Ajouter une raccourci de connexion rapide sur le bureau");
    addShortcut->setEnabled(false);
    pkcsCopyCheck = new QCheckBox("Ne pas copier la clé PKCS dans le répertoire d'OpenVPN");
    distinctConnections = new QCheckBox("Distinguer les cas de connexion depuis l'EISTI / depuis l'extérieur");
    distinctConnections->setEnabled(false);
    advancedOptionsLayout->addWidget(addShortcut);
    advancedOptionsLayout->addWidget(pkcsCopyCheck);
    advancedOptionsLayout->addWidget(distinctConnections);


    // POSITIONNEMENT
    configDetection->setLayout(configDetectionLayout);
    keyGathering->setLayout(keyGatheringLayout);
    installActions->setLayout(installActionsLayout);
    advancedOptions->setLayout(advancedOptionsLayout);

    mainLayout->addWidget(title, 1, 1, 1, 2);
    mainLayout->addWidget(configDetection, 2, 1);
    mainLayout->addWidget(keyGathering, 3, 1);
    mainLayout->addWidget(advancedOptions, 2, 2);
    mainLayout->addWidget(installActions, 3, 2);

    this->setLayout(mainLayout);

    QObject::connect(openVPNAddLocal, SIGNAL(clicked()), this, SLOT(selectInstallEvent()));
    QObject::connect(openVPNAddx64, SIGNAL(clicked()), this, SLOT(installx64Event()));
    QObject::connect(openVPNExecuteEvaluation, SIGNAL(clicked()), this, SLOT(openVPNSearchEvent()));
    QObject::connect(pkcsSelectKey, SIGNAL(clicked()), this, SLOT(selectKeyEvent()));
    QObject::connect(pkcsRetriveFromDeathstar, SIGNAL(clicked()), this, SLOT(retriveKeyEvent()));
    QObject::connect(pkcsBrowse, SIGNAL(clicked()), this, SLOT(browseKeyEvent()));
    QObject::connect(deathstarExecute, SIGNAL(clicked()), this, SLOT(deathstarEvent()));
    QObject::connect(launchInstall, SIGNAL(clicked()), this, SLOT(doInstall()));

    openVPNSearch();

}

void SetupWindow::selectInstallEvent() {
    QString ovpnExePath = QFileDialog::getExistingDirectory(this);
    QDir* tempDir = new QDir(ovpnExePath);
    if (validateInstall(tempDir))
        openVPNFoundFolders->addItem(ovpnExePath);
    else
        QMessageBox::critical(this, "Erreur", "Impossible de confirmer l'installation d'OpenVPN");
    delete tempDir;
}

void SetupWindow::openVPNSearchEvent() {
    openVPNSearch();
}

void SetupWindow::openVPNSearch() {
    openVPNFoundFolders->clear();
    QList<QDir*> foundDirs = searchForInstall();
    for (int i = 0; i < foundDirs.length(); i++) {
        if (validateInstall(foundDirs[i]))
            openVPNFoundFolders->addItem(foundDirs[i]->absolutePath());
        delete foundDirs[i];
    }

    if (openVPNFoundFolders->count() == 0) {
        openVPNDescription->setText("Aucune installation OpenVPN détectée");
        openVPNFoundFolders->addItem("Aucune version détectée");
    } else {
        openVPNDescription->setText("Sélectionnez la version d'OpenVPN à utiliser : ");
    }
}

void SetupWindow::installx64Event() {
    QDesktopServices::openUrl(QUrl("http://build.openvpn.net/downloads/releases/latest/openvpn-install-latest-stable-x86_64.exe"));
}

void SetupWindow::selectKeyEvent() {
    deathstarLogin->setEnabled(false);
    deathstarPassword->setEnabled(false);
    deathstarExecute->setEnabled(false);
    pkcsBrowse->setEnabled(true);
}

void SetupWindow::retriveKeyEvent() {
    deathstarLogin->setEnabled(true);
    deathstarPassword->setEnabled(true);
    deathstarExecute->setEnabled(true);
    pkcsBrowse->setEnabled(false);
}

void SetupWindow::deathstarEvent() {
    QDir currentDir;
    if (currentDir.exists("res/putty.exe") && currentDir.exists("res/plink.exe") && currentDir.exists("res/pscp.exe")) {
        pw = deathstarPassword->text();
        uname = deathstarLogin->text();
        QString deathstarCommand = "\"wget --no-check-certificate --keep-session-cookies --output-document=client.p12 --post-data 'username=" + uname + "&password=" + pw + "' https://vpn.eisti.fr/index.php#form\"";
        cmd1->setProgram(QString("res\\plink.exe"));
        cmd1->setArguments(QStringList() << "-ssh" << "-pw" << pw << uname + "@deathstar.eisti.fr" << deathstarCommand);
        //cmd1->setReadChannel(QProcess::StandardError);
        //cmd1->setStandardOutputFile("LOGS.txt");
        //cmd1->setStandardErrorFile("ERRORS.txt"); // /!\ MONTRE LE MDP
        QObject::connect(cmd1, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(deathstarFinishedCmd1()));
        logWindow->append(QString("Génération de la clé PKCS12 sur deathstar.eisti.fr"));
        cmd1->start();
    } else
        QMessageBox::critical(this, "Erreur", "Impossible de trouver putty.exe, plink.exe ou pscp.exe");
}

void SetupWindow::deathstarFinishedCmd1() {
    if (cmd1->state() == QProcess::NotRunning) {
        logWindow->append(QString("Commande 1 terminée"));
        cmd2->setProgram(QString("res\\pscp.exe"));
        cmd2->setArguments(QStringList() << "-pw" << pw << uname + "@deathstar.eisti.fr:client.p12" << "client.p12");
        QObject::connect(cmd2, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(deathstarFinishedCmd2()));
        logWindow->append("Récupération de la clé");
        cmd2->start();
    }
}

void SetupWindow::deathstarFinishedCmd2() {
    if (cmd2->state() == QProcess::NotRunning) {
        logWindow->append(QString("Commande 2 terminée"));
        pkcsPath = QDir().absolutePath() + QString("/client.p12");
        pkcsKeyPath->setText(pkcsPath);
        uname = "";
        pw = "";
    }
}

void SetupWindow::browseKeyEvent() {
     pkcsPath = QFileDialog::getOpenFileName(this, "Sélectionner le certificat", QString(), "Clé PKCS12 (*.p12)");
     pkcsKeyPath->setText(pkcsPath);
}

void SetupWindow::doInstall() {
    logWindow->append("Démarrage de la configuration d'OpenVPN");
    QString ovpnDirPath = openVPNFoundFolders->currentText();
    QDir ovpnDir = QDir(ovpnDirPath);
    int error = 1;

    if (ovpnDir.exists("config")) {
        logWindow->append("Migration des anciens fichiers de configuration vers conf_OLD");
        if (!ovpnDir.exists("config_OLD")) ovpnDir.mkdir("config_OLD");
        ovpnDir.cd("config");
        QStringList confFiles = ovpnDir.entryList();
        for (int i = 2; i < confFiles.length(); i++) {
            error = MoveFileExA(ovpnDir.absoluteFilePath(confFiles[i]).toUtf8(), ovpnDir.absolutePath().toUtf8() + "/../config_OLD/" + confFiles[i].toUtf8(), 1);
            if (throwError(error, "Migration du dossier config échouée")) return;
        }
        ovpnDir.cdUp();
    }

    if (ovpnDir.exists("pkcs12")) {
        logWindow->append("Migration des anciennes clés vers pkcs12_OLD");
        if (!ovpnDir.exists("pkcs12_OLD")) ovpnDir.mkdir("pkcs12_OLD");
        ovpnDir.cd("pkcs12");
        QStringList pkcsFiles = ovpnDir.entryList();
        for (int i = 2; i < pkcsFiles.length(); i++) {
            error = MoveFileExA(ovpnDir.absoluteFilePath(pkcsFiles[i]).toUtf8(), ovpnDir.absolutePath().toUtf8() + "/../pkcs12_OLD/" + pkcsFiles[i].toUtf8(), 1);
            if (throwError(error, "Migration du dossier config échouée")) return;
        }
        ovpnDir.cdUp();
    }

    logWindow->append("Créations des dossiers requis");
    if (!ovpnDir.exists("config"))
        ovpnDir.mkdir("config");
    if (!ovpnDir.exists("pkcs12"))
        ovpnDir.mkdir("pkcs12");

    logWindow->append("Personnalisation du fichier de configuration");
    if (QDir().exists("res/eisti.ovpn"))
        QDir().rmpath("res/eisti.ovpn");
    error = CopyFileA(QDir().absolutePath().toUtf8() + "/res/model.ovpn", QDir().absolutePath().toUtf8() + "/res/eisti.ovpn", false);
    if (throwError(error, "Copie du fichier de configuration échouée")) return;

    QFile config("res/eisti.ovpn");
    if (!config.open(QIODevice::Append)) {
        logWindow->append("Erreur critique : impossible de modifier eisti.ovpn");
        return;
    }

    QTextStream confStream(&config);

    if (!pkcsCopyCheck->checkState()) {
        confStream << endl << "\n" << "pkcs12 ../pkcs12/client.p12\n";
        logWindow->append("Importation de la clé PKCS12");
        error = MoveFileExA(pkcsPath.toUtf8(), ovpnDir.absolutePath().toUtf8() + "/pkcs12/client.p12", 2);
        if (throwError(error, "Importation de la clé PKCS12 échouée")) return;
    } else
        confStream << endl << "\n" << "pkcs12 " << pkcsPath << "\n";
    confStream.flush();
    config.close();

    logWindow->append("Importation du fichier de configuration");
    error = MoveFileExA(QDir().absolutePath().toUtf8() + "/res/eisti.ovpn", ovpnDir.absolutePath().toUtf8() + "/config/eisti.ovpn", 2);
    if (throwError(error, "Importation du fichier de configuration échouée")) return;

    /*if (addShortcut->checkState()) {
        logWindow->append("Création du raccourci");
        QFile batch(ovpnDir.absolutePath() + "/eisti.bat");
        if (!batch.open(QIODevice::WriteOnly)) {
            logWindow->append("Erreur critique : impossible de modifier eisti.bat");
            return;
        }
        QTextStream batchStream(&batch);
        batchStream << "cd config\n" << "..\\bin\\openvpn.exe eisti.ovpn\n";
        batchStream.flush();
        batch.close();
        system("mklink /d \"%USERPROFILE%\\Desktop\\OpenVPN EISTI\" " + ovpnDir.absolutePath().toLatin1() + "\\eisti.bat");

    }*/

    logWindow->append("Configuration terminée !");
}

bool SetupWindow::throwError(int const& errorCode, QString const& errorMsg) {
    if (errorCode == 0) {
        logWindow->append("Erreur critique : " + errorMsg);
        logWindow->append("Code d'erreur :" + QString::number(GetLastError()));
        return true;
    } else
        return false;
}

SetupWindow::~SetupWindow() {
}

/*
 *  wget --no-check-certificate --keep-session-cookies --output-document=client.p12 --post-data 'username=UTILISATEUR&password=MOTDEPASSE' https://vpn.eisti.fr/index.php#form
 *  plink.exe -ssh -pw PASSWORD USERNAME@deathstar.eisti.fr "wget --no-check-certificate --keep-session-cookies --output-document=client.p12 --post-data 'username=USERNAME&password=PASSWORD' https://vpn.eisti.fr/index.php#form"
*/
