#ifndef FUNCTIONS
#define FUNCTIONS
#include <QDir>

/*! Détermine si OpenVPN est préseant des les répertoires par défaut du système.
    Teste la présence des dossiers "Open VPN", "OpenVPN" dans "Program Files" et "Program Files (x86)".
    @return Liste des dossiers détectés. */
QList<QDir*> searchForInstall();

/*! Détermine si le dossier spécifié contient une installation OpenVPN valide.
    @param directory Objet QDir correspondant au répertoire à tester.
    @return True si le dossier contient une installation valide, False sinon. */
// TODO : Passer en pointeurs
bool validateInstall(QDir* const& directory);

#endif // FUNCTIONS

